function get(name) {
	return localStorage.getItem(name)
}

function set(name, value) {
	return localStorage.setItem(name, value)
}

window.lsm = {get, set}

module.exports = {get, set}
