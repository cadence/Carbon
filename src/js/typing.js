const {ElemJS, ejs, q} = require("./basic")
const {store} = require("./store/store")
const lsm = require("./lsm")

/**
 * Maximum number of typing users to display all names for.
 * More will be shown as "X users are typing".
 */
const maxUsers = 4

function getMemberName(mxid) {
	return store.activeRoom.value().getMemberName(mxid)
}

class Typing extends ElemJS {
	constructor() {
		super(q("#c-typing"))

		this.typingUnsubscribe = null

		this.message = ejs("span")
		this.child(this.message)

		store.activeRoom.subscribe("changeSelf", this.changeRoom.bind(this))
	}

	changeRoom() {
		if (this.typingUnsubscribe) {
			this.typingUnsubscribe()
			this.typingUnsubscribe = null
		}
		if (!store.activeRoom.exists()) return
		const room = store.activeRoom.value()
		this.typingUnsubscribe = room.timeline.typing.subscribe("changeSelf", this.render.bind(this))
		this.render()
	}

	render() {
		if (!store.activeRoom.exists()) return
		const room = store.activeRoom.value()
		let users = [...room.timeline.typing.value()]
		// don't show own typing status
		users = users.filter(u => u !== lsm.get("mx_user_id"))
		if (users.length === 0) {
			// nobody is typing
			this.removeClass("c-typing--typing")
		} else {
			let message = ""
			if (users.length === 1) {
				message = `${getMemberName(users[0])} is typing...`
			} else if (users.length <= maxUsers) {
				// feel free to rewrite this loop if you know a better way
				for (let i = 0; i < users.length; i++) {
					if (i < users.length-1) {
						message += `${getMemberName(users[i])}, `
					} else {
						message += `and ${getMemberName(users[i])} are typing...`
					}
				}
			} else {
				message = `${users.length} people are typing...`
			}
			this.class("c-typing--typing")
			this.message.text(message)
		}
	}
}

new Typing()
