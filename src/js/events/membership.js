const {UngroupableEvent} = require("./event")
const {ejs} = require("../basic")
const {extractDisplayName, resolveMxc, extractLocalpart} = require("../functions")

class MembershipEvent extends UngroupableEvent {
	constructor(data) {
		super(data)
		this.class("c-message-event")
		this.senderName = extractDisplayName(data)
		if (data.content.avatar_url) {
			this.smallAvatar = ejs("img")
				.attribute("width", "32")
				.attribute("height", "32")
				.attribute("src", resolveMxc(data.content.avatar_url, 32, "crop"))
				.class("c-message-event__avatar")
		} else {
			this.smallAvatar = ""
		}
		this.render()
	}

	static canRender(eventData) {
		return eventData.type === "m.room.member"
	}

	renderInner(iconURL, elements) {
		this.clearChildren()
		this.child(
			ejs("div").class("c-message-event__inner").child(
				iconURL ? ejs("img").class("c-message-event__icon").attribute("width", "20").attribute("height", "20").attribute("src", iconURL) : "",
				...elements
			)
		)
		super.render()
	}
}


class JoinedEvent extends MembershipEvent {
	static canRender(eventData) {
		return super.canRender(eventData) && eventData.content.membership === "join"
	}

	render() {
		const changes = []
		const prev = this.data.unsigned.prev_content
		if (prev && prev.membership === "join") {
			if (prev.avatar_url !== this.data.content.avatar_url) {
				changes.push("changed their avatar")
			}
			if (prev.displayname !== this.data.content.displayname) {
				changes.push(`changed their display name (was ${this.data.unsigned.prev_content.displayname})`)
			}
		}
		let message
		let iconURL
		if (changes.length) {
			message = " " + changes.join(", ")
			iconURL = "static/profile-event.svg"
		} else {
			message = " joined the room"
			iconURL = "static/join-event.svg"
		}
		this.renderInner(iconURL, [
			this.smallAvatar,
			this.senderName,
			message
		])
	}
}

class InvitedEvent extends MembershipEvent {
	static canRender(eventData) {
		return super.canRender(eventData) && eventData.content.membership === "invite"
	}

	render() {
		this.renderInner("static/invite-event.svg", [
			this.smallAvatar,
			`${extractLocalpart(this.data.sender)} invited ${this.data.state_key}` // full mxid for clarity
		])
	}
}

class LeaveEvent extends MembershipEvent {
	static canRender(eventData) {
		return super.canRender(eventData) && eventData.content.membership === "leave"
	}

	render() {
		this.renderInner("static/leave-event.svg", [
			this.smallAvatar,
			this.senderName,
			" left the room"
		])
	}
}

class BanEvent extends MembershipEvent {
	static canRender(eventData) {
		return super.canRender(eventData) && eventData.content.membership === "ban"
	}

	render() {
		let message =
			 ` left (banned by ${this.data.sender}`
			 + (this.data.content.reason ? `, reason: ${this.data.content.reason}` : "")
			 + ")"
		this.renderInner("static/leave-event.svg", [
			this.smallAvatar,
			this.senderName,
			message
		])
	}
}

class UnknownMembership extends MembershipEvent {
	render() {
		this.renderInner("", [
			this.smallAvatar,
			this.senderName,
			ejs("i").text(" unknown membership event")
		])
	}
}

module.exports = [JoinedEvent, InvitedEvent, LeaveEvent, BanEvent, UnknownMembership]
