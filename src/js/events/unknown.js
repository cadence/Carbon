const {GroupableEvent} = require("./event")
const {ejs} = require("../basic")

class UnknownEvent extends GroupableEvent {
	static canRender() {
		return true
	}

	render() {
		this.clearChildren()
		this.child(
			ejs("i").text(`Unknown event of type ${this.data.type}`)
		)
		super.render()
	}
}

module.exports = [UnknownEvent]

