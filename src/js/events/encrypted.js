const {GroupableEvent} = require("./event")
const {ejs} = require("../basic")

class EncryptedMessage extends GroupableEvent {
	render() {
		this.clearChildren()
		this.child(
			ejs("i").text("Carbon cannot render encrypted messages yet")
		)
		super.render()
	}

	static canRender(eventData) {
		return eventData.type === "m.room.encrypted"
	}
}

module.exports = [EncryptedMessage]
