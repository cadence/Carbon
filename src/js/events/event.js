const {ElemJS, ejs} = require("../basic")
const {dateFormatter} = require("../date-formatter")
const {SubscribeSet} = require("../store/subscribe_set.js")

class MatrixEvent extends ElemJS {
	constructor(data) {
		super("div")
		this.data = null
		this.group = null
		this.editedAt = null
		this.readBy = new SubscribeSet()
		this.update(data)
	}

	// predicates

	canGroup() {
		return false
	}

	// operations

	setGroup(group) {
		this.group = group
	}

	setEdited(time) {
		this.editedAt = time
		this.render()
	}

	update(data) {
		this.data = data
		this.render()
	}

	removeEvent() {
		if (this.group) this.group.removeEvent(this)
		else this.remove()
	}

	render() {
		this.element.classList[this.data.pending ? "add" : "remove"]("c-message--pending")
		if (this.editedAt) {
			this.child(ejs("span").class("c-message__edited").text("(edited)").attribute("title", "at " + dateFormatter.format(this.editedAt)))
		}
		return this
	}

	static canRender(eventData) {
		return false
	}
}

class GroupableEvent extends MatrixEvent {
	constructor(data) {
		super(data)
		this.class("c-message")
	}

	canGroup() {
		return true
	}
}

class UngroupableEvent extends MatrixEvent {
}

module.exports = {
	GroupableEvent,
	UngroupableEvent
}
