const {UngroupableEvent} = require("./event")
const {ejs} = require("../basic")
const lsm = require("../lsm")
const {extractDisplayName, resolveMxc, extractLocalpart} = require("../functions")

class CallEvent extends UngroupableEvent {
	constructor(data) {
		super(data)
		this.class("c-message-event")
		this.senderName = extractLocalpart(this.data.sender)
		this.render()
	}

	renderInner(iconURL, elements) {
		this.clearChildren()
		this.child(
			ejs("div").class("c-message-event__inner").child(
				iconURL ? ejs("img").class("c-message-event__icon").attribute("width", "20").attribute("height", "20").attribute("src", iconURL) : "",
				...elements
			)
		)
		super.render()
	}
}

class CallInviteEvent extends CallEvent {
	static canRender(eventData) {
		return eventData.type === "m.call.invite"
	}

	render() {
		const icon = this.data.sender === lsm.get("mx_user_id") ? "static/call-out.svg" : "static/call-in.svg"
		this.renderInner(icon, [
			this.senderName,
			" started a VOIP call, but Carbon doesn't support VOIP calls"
		])
	}
}

class CallAnswerEvent extends CallEvent {
	static canRender(eventData) {
		return eventData.type === "m.call.answer"
	}

	render() {
		this.renderInner("static/call-accepted.svg", [
			this.senderName,
			" answered the call"
		])
	}
}

class CallHangupEvent extends CallEvent {
	static canRender(eventData) {
		return eventData.type === "m.call.hangup"
	}

	render() {
		const reason = this.data.content.reason === "invite_timeout" ? "missed the call" : "hung up the call"
		this.renderInner("static/call-rejected.svg", [
			this.senderName,
			" " + reason
		])
	}
}

module.exports = [CallInviteEvent, CallAnswerEvent, CallHangupEvent]
