const {ElemJS} = require("../basic")
const {lazyLoad} = require("../lazy-load-module")

class HighlightedCode extends ElemJS {
	constructor(element) {
		super(element)
		if (this.element.tagName === "PRE" && this.element.children.length === 1 && this.element.children[0].tagName === "CODE") {
			// we shouldn't nest <code> inside <pre>. put the text in <pre> directly.
			const code = this.element.children[0]
			this.clearChildren()
			while (code.firstChild) {
				this.element.appendChild(code.firstChild)
			}
		}
		let shouldHighlight = (
			// if there are child _elements_, it's already formatted, we shouldn't mess that up
			this.element.children.length === 0
			/*
			  no need to highlight very short code blocks:
			  - content inside might not be code, some users still use code blocks
			  for plaintext quotes
			  - language detection will almost certainly be incorrect
			  - even if it's code and the language is detected, the user will
			  be able to mentally format small amounts of code themselves

			  feel free to change the threshold number
			*/
				&& this.element.textContent.length > 80
		)
		if (shouldHighlight) {
			lazyLoad("https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@10/build/highlight.min.js").then(hljs => hljs.highlightBlock(this.element))
		}
	}
}

module.exports = {HighlightedCode}
