const {ejs, ElemJS} = require("../basic")
const {resolveMxc} = require("../functions")
const {GroupableEvent} = require("./event")

class Image extends GroupableEvent {
	render() {
		this.clearChildren()
		this.class("c-message--media")
		const image = (
			ejs("img")
				.class("c-message__image")
				.attribute("src", resolveMxc(this.data.content.url))
		)
		const info = this.data.content.info
		if (info && info.w && info.h) {
			image.attribute("width", info.w)
			image.attribute("height", info.h)
		}
		const wrapper = ejs("div").class("c-media__wrapper").child(
			image
		)
		if (this.data.content.body && this.data.content.body.startsWith("SPOILER")) {
			wrapper.attribute("tabindex", 0)
			wrapper.class("c-media--spoiler")
			const wall = ejs("div").class("c-media__spoiler").text("Spoiler")
			wrapper.child(wall)
			const toggle = () => {
				wrapper.element.classList.toggle("c-media--shown")
			}
			wrapper.on("click", toggle)
			wrapper.on("keydown", event => {
				if (event.key === "Enter") toggle()
			})
		}
		this.child(wrapper)
		super.render()
	}

	static canRender(event) {
		return event.type === "m.room.message" && event.content.msgtype === "m.image"
	}

	canGroup() {
		return true
	}
}

module.exports = [Image]
