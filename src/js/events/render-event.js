const imageEvent = require("./image")
const messageEvent = require("./message")
const encryptedEvent = require("./encrypted")
const membershipEvent = require("./membership")
const unknownEvent = require("./unknown")
const callEvent = require("./call")
const hiddenEvent = require("./hidden")

const events = [
	...imageEvent,
	...messageEvent,
	...encryptedEvent,
	...membershipEvent,
	...callEvent,
	...hiddenEvent,
	...unknownEvent,
]

function renderEvent(eventData) {
	const constructor = events.find(e => e.canRender(eventData))
	return new constructor(eventData)
}

module.exports = {renderEvent}
