const {UngroupableEvent} = require("./event")

class HiddenEvent extends UngroupableEvent {
	constructor(data) {
		super(data)
		this.class("c-hidden-event")
		this.clearChildren()
	}

	static canRender(eventData) {
		return ["m.reaction", "m.call.candidates"].includes(eventData.type)
	}

	render() {
	}
}

module.exports = [HiddenEvent]
