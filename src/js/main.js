require("./focus.js")
const groups = require("./groups.js")
const chat_input = require("./chat-input.js")
const room_picker = require("./room-picker.js")
const sync = require("./sync/sync.js")
const chat = require("./chat.js")
require("./typing.js")

if (!localStorage.getItem("access_token")) {
	location.assign("./login/")
}
