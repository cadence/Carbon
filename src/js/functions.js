const lsm = require("./lsm.js")

function resolveMxc(url, size, method) {
	const match = url.match(/^mxc:\/\/([^/]+)\/(.*)/)
	if (!match) return url
	let [server, id] = match.slice(1)
	id = id.replace(/#.*$/, "")
	if (size && method) {
		return `${lsm.get("domain")}/_matrix/media/r0/thumbnail/${server}/${id}?width=${size}&height=${size}&method=${method}`
	} else {
		return `${lsm.get("domain")}/_matrix/media/r0/download/${server}/${id}`
	}
}

function extractLocalpart(mxid) {
	// try to extract the localpart from the mxid
	let match = mxid.match(/^@([^:]+):/)
	if (match) {
		return match[1]
	}
	// localpart extraction failed, use the whole mxid
	return mxid
}

function extractDisplayName(stateEvent) {
	const mxid = stateEvent.state_key
	// see if a display name is set
	if (stateEvent.content.displayname) {
		return stateEvent.content.displayname
	}
	// fall back to the mxid
	return extractLocalpart(mxid)
}

module.exports = {
	resolveMxc,
	extractLocalpart,
	extractDisplayName
}
