const {Subscribable} = require("./subscribable.js")
const {SubscribeMapList} = require("./subscribe_map_list.js")
const {SubscribeSet} = require("./subscribe_set.js")
const {SubscribeValue} = require("./subscribe_value.js")

const store = {
	groups: new SubscribeMapList(SubscribeValue),
	rooms: new SubscribeMapList(SubscribeValue),
	directs: new SubscribeSet(),
	activeGroup: new SubscribeValue(),
	activeRoom: new SubscribeValue(),
	newEvents: new Subscribable(),
	notificationsChange: new Subscribable()
}

window.store = store

module.exports = {store}
