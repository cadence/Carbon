const {Subscribable} = require("./subscribable.js")

class SubscribeMap extends Subscribable {
	constructor(inner) {
		super()
		this.inner = inner
		Object.assign(this.events, {
			addItem: [],
			editItem: [],
			deleteItem: [],
			changeItem: [],
			askSet: []
		})
		Object.assign(this.eventDeps, {
			addItem: ["changeItem"],
			editItem: ["changeItem"],
			deleteItem: ["changeItem"],
			changeItem: [],
			askSet: []
		})
		this.map = new Map()
	}

	has(key) {
		return this.map.has(key) && this.map.get(key).exists()
	}

	get(key) {
		if (this.map.has(key)) {
			return this.map.get(key)
		} else {
			const item = new this.inner()
			this.map.set(key, item)
			return item
		}
	}

	forEach(f) {
		for (const entry of this.map.entries()) {
			f(entry[0], entry[1])
		}
	}

	askSet(key, value) {
		this.broadcast("askSet", {key, value})
	}

	set(key, value) {
		let s
		if (this.map.has(key)) {
			const exists = this.map.get(key).exists()
			s = this.map.get(key).set(value)
			if (exists) {
				this.broadcast("editItem", key)
			} else {
				this.broadcast("addItem", key)
			}
		} else {
			s = new this.inner().set(value)
			this.map.set(key, s)
			this.broadcast("addItem", key)
		}
		return s
	}

	delete(key) {
		if (this.backing.has(key)) {
			this.backing.delete(key)
			this.broadcast("deleteItem", key)
		}
	}
}

module.exports = {SubscribeMap}
