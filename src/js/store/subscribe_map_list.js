const {Subscribable} = require("./subscribable.js")
const {SubscribeValue} = require("./subscribe_value.js")

class SubscribeMapList extends Subscribable {
	constructor(inner) {
		super()
		this.inner = inner
		Object.assign(this.events, {
			addItem: [],
			deleteItem: [],
			editItem: [],
			changeItem: [],
			askAdd: []
		})
		Object.assign(this.eventDeps, {
			addItem: ["changeItem"],
			deleteItem: ["changeItem"],
			editItem: ["changeItem"],
			changeItem: [],
			askAdd: []
		})
		this.map = new Map()
		this.list = []
	}

	has(key) {
		return this.map.has(key) && this.map.get(key).exists()
	}

	get(key) {
		if (this.map.has(key)) {
			return this.map.get(key)
		} else {
			const item = new this.inner()
			this.map.set(key, item)
			return item
		}
	}

	forEach(f) {
		this.list.forEach(key => f(key, this.get(key)))
	}

	askAdd(key, data) {
		this.broadcast("askAdd", {key, data})
	}

	addStart(key, value) {
		this._add(key, value, true)
	}

	addEnd(key, value) {
		this._add(key, value, false)
	}

	sort() {
		const key = this.list[0]
		if (typeof this.map.get(key).value().order === "number") {
			this.sortByNumber()
		} else {
			this.sortByString()
		}
	}

	sortByNumber() {
		this.list.sort((a, b) => {
			const orderA = this.map.get(a).value().order
			const orderB = this.map.get(b).value().order
			return orderA - orderB
		})
		this.broadcast("changeItem")
	}

	sortByString() {
		this.list.sort((a, b) => {
			const orderA = this.map.get(a).value().order
			const orderB = this.map.get(b).value().order
			if (orderA < orderB) return -1
			else if (orderA > orderB) return 1
			else return 0
		})
		this.broadcast("changeItem")
	}

	_add(key, value, start) {
		let s
		if (this.map.has(key)) {
			const exists = this.map.get(key).exists()
			s = this.map.get(key).set(value)
			if (exists) {
				this.broadcast("editItem", key)
			} else {
				this.broadcast("addItem", key)
			}
		} else {
			s = new this.inner().set(value)
			this.map.set(key, s)
			if (start) this.list.unshift(key)
			else this.list.push(key)
			this.broadcast("addItem", key)
		}
		return s
	}
}

module.exports = {SubscribeMapList}
