const {ElemJS} = require("./basic.js")

class Anchor extends ElemJS {
	constructor() {
		super("div")
		this.class("c-anchor")
	}

	scroll() {
		// console.log("anchor scrolled")
		this.element.scrollIntoView({block: "start"})
	}
}

module.exports = {Anchor}
