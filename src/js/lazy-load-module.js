// I hate this with passion
async function lazyLoad(url) {
	const cache = window.lazyLoadCache || new Map()
	window.lazyLoadCache = cache
	if (cache.get(url)) return cache.get(url)

	const module = loadModuleWithoutCache(url)
	cache.set(url, module)
	return module
}

// Loads the module without caching
async function loadModuleWithoutCache(url) {
	const src = await fetch(url).then(r => r.text())
	let module = {}
	eval(src)
	return module.exports
}

module.exports = {lazyLoad}
