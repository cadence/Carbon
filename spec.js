module.exports = [
	{
		type: "file",
		source: "/assets/fonts/whitney-500.woff",
		target: "/static/whitney-500.woff",
	},
	{
		type: "file",
		source: "/assets/fonts/whitney-400.woff",
		target: "/static/whitney-400.woff",
	},
	{
		type: "bundle",
		source: "/js/login.js",
		target: "/static/login.js",
	},
	{
		type: "bundle",
		source: "/js/main.js",
		target: "/static/bundle.js"
	},
	{
		type: "file",
		source: "/assets/fonts/whitney-500.woff",
		target: "/static/whitney-500.woff",
	},
	{
		type: "file",
		source: "/assets/icons/directs.svg",
		target: "/static/directs.svg",
	},
	{
		type: "file",
		source: "/assets/icons/channels.svg",
		target: "/static/channels.svg",
	},
	{
		type: "file",
		source: "/assets/icons/join-event.svg",
		target: "/static/join-event.svg",
	},
	{
		type: "file",
		source: "/assets/icons/leave-event.svg",
		target: "/static/leave-event.svg",
	},
	{
		type: "file",
		source: "/assets/icons/invite-event.svg",
		target: "/static/invite-event.svg",
	},
	{
		type: "file",
		source: "/assets/icons/profile-event.svg",
		target: "/static/profile-event.svg",
	},
	{
		type: "file",
		source: "/assets/icons/call-out.svg",
		target: "/static/call-out.svg",
	},
	{
		type: "file",
		source: "/assets/icons/call-in.svg",
		target: "/static/call-in.svg",
	},
	{
		type: "file",
		source: "/assets/icons/call-accepted.svg",
		target: "/static/call-accepted.svg",
	},
	{
		type: "file",
		source: "/assets/icons/call-rejected.svg",
		target: "/static/call-rejected.svg",
	},
	{
		type: "sass",
		source: "/sass/main.sass",
		target: "/static/main.css",
	},
	{
		type: "sass",
		source: "/sass/login.sass",
		target: "/static/login.css",
	},
	{
		type: "pug",
		source: "/home.pug",
		target: "/index.html",
	},
	{
		type: "pug",
		source: "/login.pug",
		target: "/login/index.html",
	},
];
