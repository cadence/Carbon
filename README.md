# Carbon Chat

Carbon is the Matrix client for Discord and Guilded refugees.

Visit the hosted instance on
[https://carbon.chat](https://carbon.chat).

## Status

Carbon is **abandoned** by its author, but it is still solid code to build on for anyone with the time and inclination to pick it up.

## Report bugs and suggest features

Please briefly check this README and the issues page first to make
sure that the issue/feature is not already known!

- If you already have an account on Gitdab, use the issues page.
- If you don't have an account, and don't wish to create one, you can
send an email to the [mailing list].

If something in the interface isn't working as you think it should,
please provide a screenshot of any messages from the browser devtools
console. If using the mailing list, attachments aren't supported, so
you'll have to upload to some image host and post the link.

[mailing list]: https://lists.sr.ht/~cadence/carbon-discuss

## The dream

Carbon's planned features, compared to Discord and Guilded:

- End to end encryption
- Free of charge, per-account, custom emojis and custom emoji packs
- No limit to number of groups you can join at a time
- Uses the open Matrix and Mumble systems
- Much better IRC layout
- Probably more

Carbon's planned features, compared to Element:

- Get rid of the unmanageable combined rooms list
- Groups like Discord/Guilded guilds
  - Always have one group selected at a time
  - Synchronised membership, moderators, power levels and bans
  - Ordered channel list
  - Unread indicators
- Add existing channels to groups
- Pin any channel to the groups bar
- Tidy Mumble integration to add voice channels
- More reliable unreads
- Per-account custom emojis (Ponies+FluffyChat integration) and custom emoji packs
- Slightly better IRC layout
- Probably more

## The reality

Carbon is currently _technically_ usable as a chat app, but is very
early in development. These important features still need to be
implemented:

- Emojis
- Reactions
- Encryption
- Groups v2
- Group management
- Pinned channels
- Mumble integration

For more information, see [issue
#10.](https://gitdab.com/cadence/Carbon/issues/10)

## The code

### Downloading a CI build

Visit [drone CI](https://drone.badat.dev/cadence/Carbon/branches),
select the branch you want to use, select `b2` on the left, scroll
down, and open the URL on the last line to download the build.

### Building from source yourself

Dependencies:

- git
- node
- npm (bundled with node)

Build:

    npm install -D
    npm run rebuild

### Hosting a build

Send the files from the `build` folder to a static file server. Apply
a long cache-control header to everything served under `/static`, and
no cache-control header to everything else.

### Developing

    npm run watch

Files will be rebuilt as you save them.

Use `python3 -m http.server -d build` to serve the build on
[http://localhost:8000](http://localhost:8000).

(Avoid `npx http-server`, it caches too much stuff.)
